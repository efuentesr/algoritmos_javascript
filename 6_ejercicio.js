/*
Leer un Número enteros de dos digitos y determinar si un digito es múltiplo del otro.
*/

var numero=93;


if(numero>=10 && numero< 100){
    
    const digito1= parseInt(numero/10);
    console.log(digito1)
    const digito2=numero%10;
    console.log(digito2)

    if((digito1%digito2==0) || (digito2%digito1==0)){
        console.log("uno de los digitos es multiplo del otro digito");
    }else{
        console.log("ninguno de los digitos es multiplo del otro digito");
    }
}else{
    console.log("los numeros ingresados deben ser de dos digitos");
}