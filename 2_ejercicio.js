/*
Leer un Número entero de dos digitos y terminar a cuánto es igual la suma de sus digitos.
*/

var numero=96;

if(numero>=10 && numero< 100){
    const primer_digito=parseInt(numero/10);
    const segundo_digito=parseInt(numero%10);

    const suma_digitos=primer_digito+segundo_digito;
    console.log("La suma de ", primer_digito, segundo_digito, "=",suma_digitos);
}else{
    console.log("El valor ingresado no es un numero de dos digitos");
}