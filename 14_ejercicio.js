/*
Leer un número entero y mostrar en pantalla su tabla de multiplicar
*/

let tabla=5;
console.log("Tablas de Multiplicar Del ",tabla);


generaTabla(tabla);


function generaTabla(tabla) {
        console.log("Imprimiendo tabla del ", tabla);        

        for (let factor=1; factor<=10; factor++){
            console.log(`${tabla} , X , ${factor}, = ,${tabla*factor}`);   
        }    

}
