/*

Leer un número entero de dos digitos y determinar si es primo y además si es negativo.
*/

var numero=53;
var num_divisores=2;
var max_div_posible=parseInt(numero/2);

if (numero<100){
    
    for (var i=2;i<=max_div_posible; i=i+1){
        if ((numero%i)==0)
            num_divisores++; 

    }
    
    if(num_divisores<3){
        
            console.log(`El numero ${numero} es primo `);
    }else{
            console.log(`El numero ${numero} no es primo`);
    }

    if(numero<0){
        console.log(`El numero ${numero} es negativo`);
    }else{
        console.log(` y El numero ${numero} es positivo`);

    }

}  else {

    console.log("El valor debe ser menor de 100");
}