/*
Generar todas las tablas de multiplicar del 1 al 10
*/

console.log("Tablas de Multiplicar del 1 al 10")


for (var tabla=1; tabla<= 10; tabla++){
    console.log("La Tabla del :",tabla);
    for (let factor=1; factor<=10; factor++){
        console.log(`${tabla} , X , ${factor}, = ,${tabla*factor}`);   
    }

}