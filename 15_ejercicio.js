/*
Leer 10 números enteros, almacenarlos en un vector y determinar en qué posiciones se encuentra el número mayor.
*/

let lista_numeros=[3,5,2,97,10,45,20,78,83,34];

console.log(lista_numeros.length);


let valor_mayor=mayorLista(lista_numeros);
let indice_del_mayor=lista_numeros.indexOf(valor_mayor);

console.log("de la lista : ",lista_numeros)

console.log(`el indice del numero ${valor_mayor} mayor de la lista es  : ${indice_del_mayor}`);


function mayorLista(lista_num) {
             
    mayor=0    
        for (let indice=0; indice<lista_num.length; indice++){
            if (lista_num[indice]>mayor) {
               mayor=lista_num[indice];
            } 
             
        }   
        return mayor;  

}