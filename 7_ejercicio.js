/*Leer un número entero de dos digitos y determinar si los dos digitos son iguales.

*/

var numero1=77;

if(numero1>=10 && numero1< 100){
    const digito1= parseInt(numero1/10);
    const digito2=numero1%10;
    if(digito1==digito2){
        console.log("Los digitos del numero son iguales");
    }else{
        console.log("Los digitos del numero son diferentes");
    }
}else{
    console.log("Los numeros ingresados deben ser de dos digitos");
}