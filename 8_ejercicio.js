/*

Leer un número entero de 4 digitos y determinar si el segundo digito es igual al penúltimo.
 
*/


numero=6775;


let primera_cifra=parseInt(numero/1000);
let residuo_unid_mil=numero%1000;
let segunda_cifra =parseInt(residuo_unid_mil/100);
let residuo_centenas=residuo_unid_mil%100;
let tercera_cifra =parseInt(residuo_centenas/10);

if (segunda_cifra==tercera_cifra){
       console.log(`El segundo digito ${segunda_cifra} y penultimo  digito ${tercera_cifra} son iguales`);
}else{
       console.log(`El segundo digito ${segunda_cifra} y penultimo digito ${tercera_cifra} son diferentes`);
    }