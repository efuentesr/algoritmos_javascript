/*
Leer un número entero y determinar a cuánto es igual la suma de sus digitos.
*/

let numero=232371;
let cadena_numero=String(numero);


let suma_digitos=sumardigitos(cadena_numero);


console.log(`La Suma de los digitos del numero  ${numero}  es : ${suma_digitos}`);


function sumardigitos(lista_cadena) {
           
    let acum_digitos=0    ;
        for (let indice=0; indice<lista_cadena.length; indice++){
            
               acum_digitos=acum_digitos+ Number(lista_cadena[indice]);
            } 
            return acum_digitos; 

        }