/*

Leer un número entero de dos digitos menor que 20 y determinar si es primo
*/


var numero =19;
var num_divisores=2;
var max_div_posibles=parseInt(numero/2);

if (numero<20){

    for(var i=2;i<=max_div_posibles;i++){
        if ((numero%i)==0){
            num_divisores++;
        }    
    }

    if(num_divisores<3){
        
            console.log("El numero es primo");
    }else{
            console.log("El numero no es primo");
    }
} else {

    console.log("El numero debe ser menor de 20");
}
